package sample;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField inputIntNumber1;

    @FXML
    private TextField inputIntNumber2;

    @FXML
    private TextField amount;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClear;



    @FXML
    void add(ActionEvent event) {
        if (inputIntNumber1.getText().isEmpty() || inputIntNumber2.getText().isEmpty()) {
            Shake number1Anim = new Shake(inputIntNumber1);
            Shake number2Anim = new Shake(inputIntNumber2);
            number1Anim.playAnimation();
            number2Anim.playAnimation();
            return;
        }
        int first = Integer.parseInt(inputIntNumber1.getText());
        int second = Integer.parseInt(inputIntNumber2.getText());
        int sum = first + second;
        amount.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        inputIntNumber1.clear();
        inputIntNumber2.clear();
        amount.clear();
    }
}



